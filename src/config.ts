import * as convict from 'convict';

const config = convict({
  env: {
    doc: 'The application environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  db: {
    user: {
      doc: 'Database user',
      format: 'String',
      default: 'test',
    },
    password: {
      doc: 'Database password',
      format: 'String',
      default: '',
    },
    host: {
      doc: 'Database host',
      format: 'String',
      default: 'test host',
    },
    name: {
      doc: 'Database name',
      format: 'String',
      default: '',
    },
  },
});

config.loadFile(['.env.json']);
config.validate({allowed: 'strict'});

export default config;
