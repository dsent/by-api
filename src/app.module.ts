import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MainModule } from './main/main.module';
import { ExerciseModule } from './exercise/exercise.module';
import config from './config';

@Module({
  imports: [
    MainModule,
    ExerciseModule,
    MongooseModule.forRoot(config.get('db.host') + config.get('db.name'), { useNewUrlParser: true, useUnifiedTopology: true }),
  ],
})
export class AppModule {}
