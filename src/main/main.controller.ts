import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import config from '../config';

@ApiTags('Main')
@Controller('main')
export class MainController {
  @Get('one')
  getMainData(): string {
    return config.get('db.host');
  }

  @Get('two')
  getMainDataTwo(): string {
    return 'Main Data Two';
  }
}
