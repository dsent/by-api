import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ApiParam, ApiProperty, ApiTags } from '@nestjs/swagger';

import { ExerciseDto } from './dto';

@ApiTags('Exercise')
@Controller('exercise')
export class ExerciseController {
  @Post()
  create(@Body() exerciseDto: ExerciseDto): string {
    return 'create';
  }

  @Get()
  findAll(): string {
    return 'findAll';
  }

  @Get(':id')
  @ApiParam({ name: 'id' })
  findOne(@Param() params): string {
    return `findOne - ${params.id}`;
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() exerciseDto: ExerciseDto) {
    return `This action updates a #${id} cat`;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return `This action removes a #${id} cat`;
  }
}
