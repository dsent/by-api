import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ExerciseController } from './exercise.controller';
import { ExerciseSchema } from './schemas';

@Module({
  controllers: [ExerciseController],
  imports: [
    MongooseModule.forFeature([
      { name: 'Exercise', schema: ExerciseSchema },
    ]),
  ],
})
export class ExerciseModule {}
