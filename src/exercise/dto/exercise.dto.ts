import { ApiProperty } from '@nestjs/swagger';

export class ExerciseDto {
  @ApiProperty()
  readonly name: string;

  @ApiProperty()
  readonly description: string;

  @ApiProperty()
  readonly difficulty: string;
}
