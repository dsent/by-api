import * as mongoose from 'mongoose';

export const ExerciseSchema = new mongoose.Schema({
  name: String,
  difficulty: {
    type: String,
    enum: ['simple', 'medium', 'hard'],
  },
});
